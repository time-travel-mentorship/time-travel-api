# Epoch Time Travel System


## Business rules

* Can only carry 3 passengers at a time
* Can only travel to 7 different eras: Prehistory, the Antiquity, the Middle Ages, the Present, the Apocalypse, the Future, and the End of Time
* Can either travel throught time or space, not both at a time
* Some locations only exists in certain eras, trips to invalid locations are not allowed

| Era\Location | Guardia Castle | Sun Shrine | Ioka Village | Proto Dome | Forest | End of Time |
| ------------ |:--------------:|:----------:|:------------:|:----------:|:------:|:-----------:|
| Prehistory   |                |      x     |       x      |            |    x   |             |
| Antiquity    |                |      x     |              |            |    x   |             |
| Middle Ages  |       x        |      x     |              |            |    x   |             |
| Present      |       x        |      x     |              |            |    x   |             |
| Apocalypse   |       x        |      x     |              |            |    x   |             |
| Future       |                |      x     |              |      x     |        |             |
| End of Time  |                |            |              |            |        |      x      |