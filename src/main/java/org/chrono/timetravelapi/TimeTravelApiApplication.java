package org.chrono.timetravelapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimeTravelApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeTravelApiApplication.class, args);
	}
}
