package org.chrono.timetravelapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;

@RestController
public class TimeTravelController {

    @Autowired
    private TripRepository tripRepository;

    @RequestMapping(value = "eras", method = RequestMethod.GET)
    public Collection<String> getErasAvailable() {
        return Collections.singleton("Prehistory, Antiquity, Middle Ages, Present, Apocalypse, Future, End of Time");
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public Long registerTrip(@RequestBody Trip trip) {
        Trip savedTrip = tripRepository.save(trip);
        return savedTrip.getId();
    }
}
