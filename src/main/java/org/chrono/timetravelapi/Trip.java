package org.chrono.timetravelapi;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Trip {

    @Id
    @GeneratedValue
    private Long id;
    private String originEra;
    private String destinationEra;
    private String originPlace;
    private String destinationPlace;
}
